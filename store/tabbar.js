import {
	publicBar
} from "@/utils/tabbarlist.js"
export default {
	namespaced: true, // 为模块添加命名空间，这样在外部可以根据map来映射模块方法属性
	state: () => ({
		/**
		 * 导航栏的数组，用来存导航栏中每个商品的信息对象
		 * 每个信息对象，都包含如下几个属性：
		 * { pagePath,iconPath,selectedIconPath,text,name }
		 */
		tabBarList: publicBar,
		/**
		 * 是否保持到下次进来跳转上次路由
		 * activeIndex: uni.getStorageSync('acIndex') || 'home', //本地化index 控制页面刷新导航高亮位置不变
		 */
		activeIndex: 'home', //本地化index 控制页面刷新导航高亮位置不变
	}),
	mutations: {
		//用户改变当前高亮的导航栏后进行记录
		changeIndex(state, index) {
			uni.setStorageSync('acIndex', index)
			state.activeIndex = index
		},
		//根据用户信息的改变俩获取到最新的导航栏数据
		changeBarList(state, obj) {
			state.tabBarList = obj
		}
	},
	getters: {},
	actions: {}
}