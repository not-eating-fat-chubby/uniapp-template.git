// 页面路径：store/index.js 
import Vue from 'vue'
import Vuex from 'vuex'
// 1、导入导航栏tabbar的 vuex 模块
import moduleTabbar from './tabbar.js'
import moduleUser from './user.js'
Vue.use(Vuex); //vue的插件机制
//Vuex.Store 构造器选项
const store = new Vuex.Store({
	state: {},
	modules: {
		/**
		 * 挂载导航栏的 vuex 模块，模块内成员的访问路径被调整为 m_tabbar 例如：
		 * 导航栏模块中 tabbar 数组的访问路径是 m_tabbar/tabbar
		 */
		'm_tabbar': moduleTabbar,
		'm_user': moduleUser,
	},
	mutations: {},
	getters: {},
	actions: {}
})
export default store