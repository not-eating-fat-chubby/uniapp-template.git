import publicBar from "@/utils/tabbarlist.js"
export default {
	namespaced: true, // 为模块添加命名空间，这样在外部可以根据map来映射模块方法属性
	state: () => ({
		userInfo: uni.getStorageSync('userInfo') || {},
	}),
	mutations: {
		//用户登录后触发次方法存储用户信息
		updateUserInfo(state, obj) {
			uni.setStorageSync('userInfo', obj)
			state.userInfo = obj;
		}
	},
	getters: {},
	actions: {
		//用户登录后触发次方法存储用户信息
		setUserInfo({
			state,
			commit
		}, obj) {
			commit('updateUserInfo', obj);
			//判断用户是否登录，登录后将个人导航栏加入要展示的数组中(大家根据需求来做)
			if (obj.role == 'admin') {
				const data = publicBar.filter(c => c.role.filter(j => j === obj.role).length);
				commit('m_tabbar/changeBarList', data, {
					root: true
				})
			}
		}
	}
}