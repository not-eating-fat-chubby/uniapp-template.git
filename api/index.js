/**
 * api接口的统一出口
 */
import common from './common'
import login from './login'

export default {
	common,
	login,
}