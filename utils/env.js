// 需要改变环境注释这一个地方就行
let env = "dev"; // 开发环境
// const env = "pro"; // 生产环境

if (process.env.NODE_ENV==='development') {
	env='dev'
} else if(process.env.NODE_ENV==='production'){
	env='pro'
}
const url = require(`@/env/${env}.json`);
export default {
	"ENV_NAME": env,
	...url
}