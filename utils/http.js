import util from './util';
import env from './env';

const showLog = false;
let config = {
	// 请求的公共url
	baseUrl: env.BASE_URL, // 并非真实接口地址，换成自己的接口地址
	header: {
		'Content-Type': 'application/json'
	},
	data: {},
	method: "GET",
	dataType: "json",
	/* 如设为json，会对返回的数据做一次 JSON.parse */
	responseType: "text",
	success() {},
	fail() {},
	complete() {}
};

/**
 * 可以自己配置新的请求后端地址
 * https://www.uvui2.cn/
 * 注意最后面添加 "/"
 */
if (uni.getStorageSync('newBaseUrl')) {
	config.baseUrl = 'https://www.uvui2.cn/'
}

// 封装数据返回成功提示函数------------------------------------------------------
function successState(res) {
	let code = res.code;
	if (showLog) {
		console.log('@return-data:')
		console.log(res)
	}
	//公共报错提醒
	if (code !== 0) {
		const msg = res.data.message ? res.data.message : '网络请求错误，请联系管理员'
		// 非成功状态码弹窗
		uni.showToast({
			icon: 'none',
			duration: 3000,
			title: `${msg}`
		});
	}
	// 登陆失效
	if (res.data.code === 403) {
		// 清除本地token
		removeToken()
		// 关闭所有页面返回到登录页
		uni.reLaunch({
			url: '/pages/login/login'
		})
	}
	if (showLog) {
		console.log('/------http(END)------/')
	}
	return res
}
// 封装数据返回失败提示函数------------------------------------------------------
function errorState(err) {
	// 请求失败弹窗
	uni.showToast({
		icon: 'none',
		duration: 3000,
		title: '服务器错误,请稍后再试'
	});
	if (showLog) {
		console.log('/------http(END)------/')
	}
}


// 封装axios---------------------------------------------------------------------
function service(options = {}) {
	const nowurl = options.url ? `?api=${options.url}&domain=sipgl_lcl` : '?domain=sipgl_lcl'
	options.url = `${env.BASE_URL}${nowurl}`
	// 判断本地是否存在token，如果存在则带上请求头
	if (util.getToken()) {
		options.header = {
			'Content-type': 'application/json',
			'Authorization': `${util.getToken()}` // 这里是token
		};
	}
	if (showLog) {
		console.log('/------http(START)------/')
		console.log('@all-url:')
		console.log(options.url)
		console.log('@params:')
		console.log(options)
	}

	return new Promise((resolved, rejected) => {
		options.success = (res) => {
			successState(res.data)
			resolved(res.data)
		};
		options.fail = (err) => {
			errorState(err.data)
			rejected(err.data);
		};
		const _config = Object.assign({}, config, options);
		uni.request(_config);
	});
}

export default service;