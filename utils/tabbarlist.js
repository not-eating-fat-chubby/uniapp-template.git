import rukudan from "@/static/tabicon/rukudan.png"
import rukudanactive from "@/static/tabicon/rukudanactive.png"
import kucunpandian2 from "@/static/tabicon/kucunpandian2.png"
import kucunpandianactive2 from "@/static/tabicon/kucunpandianactive2.png"
import zhanghao from "@/static/tabicon/zhanghao.png"
import zhanghaoactive from "@/static/tabicon/zhanghaoactive.png"
import zhuangche from "@/static/tabicon/zhuangche.png"
import zhuangcheactive from "@/static/tabicon/zhuangcheactive.png"

const publicBar = [{
		"pagePath": "pages/tabbar/work-space",
		"iconPath": rukudan,
		"selectedIconPath": rukudanactive,
		"text": "报工",
		"name": "work-space",
		"role": ['user', 'admin']
	},
	{
		"pagePath": "pages/tabbar/home",
		"iconPath": kucunpandian2,
		"selectedIconPath": kucunpandianactive2,
		"text": "首页",
		"name": "home",
		"role": ['user', 'admin']
	},
	{
		"pagePath": "pages/tabbar/user-center",
		"iconPath": zhanghao,
		"selectedIconPath": zhanghaoactive,
		"text": "我的",
		"name": "user-center",
		"role": ['user', 'admin']
	},
	{
		"pagePath": "pages/tabbar/about",
		"iconPath": zhuangche,
		"selectedIconPath": zhuangcheactive,
		"text": "关于",
		"name": "about",
		"role": ['admin']
	}
]

export default publicBar