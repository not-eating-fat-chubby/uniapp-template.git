const TokenKey = 'uni_token';
const utils = {
	removeStorage(key = '') {
		if (key) {
			return uni.removeStorageSync(key)
		}
	},
	setStorage(key = '', val) {
		if (key) {
			return uni.setStorageSync(key, val)
		}
	},
	getStorage(key = '') {
		if (key) {
			return uni.getStorageSync(key)
		}
	},
	removeToken() {
		return uni.removeStorageSync(TokenKey)
	},
	setToken(token) {
		return uni.setStorageSync(TokenKey, token)
	},
	getToken() {
		return uni.getStorageSync(TokenKey)
	}
}
export default utils