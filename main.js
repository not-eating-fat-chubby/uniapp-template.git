import App from './App'

// #ifndef VUE3
import Vue from 'vue'
import './uni.promisify.adaptor'
Vue.config.productionTip = false
// api请求
import api from './api'
Vue.prototype.$api = api

// 公共方法
import util from './utils/util.js'
Vue.prototype.$util = util

// 全局store
import store from './store/index.js'
Vue.prototype.$store = store

App.mpType = 'app'
const app = new Vue({
	...App,
	store
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif